import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <section className='planets'>
    <h1 className='planets-heading'>Solar system planets</h1>
    <label className="switch" htmlFor="checkbox">
      <input type="checkbox" id="checkbox" />
      <div className="slider round" onClick={function changeTheme() {
        let element = document.body;
        element.classList.toggle("dark");
      }}></div>
    </label>
  <ul>
    <li className='planets-list'>Mercury</li>
    <li className='planets-list'>Venus</li>
    <li className='planets-list'>Earth</li>
    <li className='planets-list'>Mars</li>
    <li className='planets-list'>Jupiter</li>
    <li className='planets-list'>Saturn</li>
    <li className='planets-list'>Uranus</li>
    <li className='planets-list'>Neptune</li>
  </ul>
  </section>
);
